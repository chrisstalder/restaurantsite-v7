function validateForm() {
    var name = document.forms["contactForm"]["first"].value;

    if (name == null || name == "") {
        alert("You must enter a name");
        return false;
    }

    var phone = document.forms["contactForm"]["phone"].value;
    var email = document.forms["contactForm"]["email"].value;

    if (phone == null || phone ==""){
      if (email == null || email == ""){
        alert("You must enter either a phone number or email address");
        return false;
      }
    }

    if (email == null || email ==""){
      if (phone == null || phone == ""){
        alert("You must enter either a phone number or email address");
        return false;
      }
    }

    var e = document.getElementById("ddlView");
    var reason = e.options[e.selectedIndex].text;
    var addInfo = document.contactForm.addInfo.value;

    if(reason=="Other"){
      if (addInfo == null || addInfo == ""){
        alert("Please provide us with additional info about your inquiry");
        return false;
      }
    }

    var day1 = document.getElementById("day1").checked;
    var day2 = document.getElementById("day2").checked;
    var day3 = document.getElementById("day3").checked;
    var day4 = document.getElementById("day4").checked;
    var day5 = document.getElementById("day5").checked;

    if (day1 == false && day2 == false && day3 == false && day4 == false && day5 == false){
			alert('Please provide us with the best days to contact you.');
			return false;
		}

}
